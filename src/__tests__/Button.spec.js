// describe("Button component", () => {
//   test("it shows the expected text when clicked (testing the wrong way!)", () => {
//     const button = create(<Button />);
//     expect(button.toJSON()).toMatchSnapshot();
//     const component = create(<Button text="SUBSCRIBE TO BASIC" />);
//     const instance = component.getInstance();
//     expect(instance.state.text).toBe("");
//     instance.handleClick();
//     expect(instance.state.text).toBe("PROCEED TO CHECKOUT");
//   });
// });

// import React from "react";
// import { create } from "react-test-renderer";

// class Button extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = { text: "" };
//     this.handleClick = this.handleClick.bind(this);
//   }

//   handleClick() {
//     this.setState(() => {
//       return { text: "PROCEED TO CHECKOUT" };
//     });
//   }

//   render() {
//     return (
//       <button onClick={this.handleClick}>
//         {this.state.text || this.props.text}
//       </button>
//     );
//   }
// }

// describe("Button component", () => {
//   test("it shows the expected text when clicked (testing the wrong way!)", () => {
//     const component = create(<Button text="SUBSCRIBE TO BASIC" />);
//     const instance = component.root;
//     const button = instance.findByType("button");
//     button.props.onClick();
//     expect(button.props.children).toBe("PROCEED TO CHECKOUT");
//   });
// });

// import React, { useState } from "react";
// import { create, act } from "react-test-renderer";

// function Button(props) {
//   const [text, setText] = useState("");
//   function handleClick() {
//     setText("PROCEED TO CHECKOUT");
//   }
//   return <button onClick={handleClick}>{text || props.text}</button>;
// }

// describe("Button component", () => {
//   test("it shows the expected text when clicked", () => {
//     let component;
//     act(() => {
//       component = create(<Button text="SUBSCRIBE TO BASIC" />);
//     });
//     const instance = component.root;
//     const button = instance.findByType("button");
//     act(() => button.props.onClick());
//     expect(button.props.children).toBe("PROCEED TO CHECKOUT");
//   });
// });

// import React, { useState } from "react";
// import ReactDOM from "react-dom";
// import { act } from "react-dom/test-utils";

// let container;

// beforeEach(() => {
//   container = document.createElement("div");
//   document.body.appendChild(container);
// });

// afterEach(() => {
//   document.body.removeChild(container);
//   container = null;
// });

// function Button(props) {
//   const [text, setText] = useState("");
//   function handleClick() {
//     setText("PROCEED TO CHECKOUT");
//   }
//   return <button onClick={handleClick}>{text || props.text}</button>;
// }

// describe("Button component", () => {
//   test("it shows the expected text when clicked", () => {
//     act(() => {
//       ReactDOM.render(<Button text="SUBSCRIBE TO BASIC" />, container);
//     });
//     const button = container.getElementsByTagName("button")[0];
//     expect(button.textContent).toBe("SUBSCRIBE TO BASIC");
//     act(() => {
//       button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
//     });
//     expect(button.textContent).toBe("PROCEED TO CHECKOUT");
//   });
// });

import React, { Component } from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

let container;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("User component", () => {
  test("it shows a list of users", async () => {
    const fakeResponse = [{ name: "John Doe" }, { name: "Kevin Mitnick" }];

    jest.spyOn(window, "fetch").mockImplementation(() => {
      const fetchResponse = {
        json: () => Promise.resolve(fakeResponse)
      };
      return Promise.resolve(fetchResponse);
    });

    await act(async () => {
      render(<Users />, container);
    });

    expect(container.textContent).toBe("John DoeKevin Mitnick");

    window.fetch.mockRestore();
  });
});

export default class Users extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then(response => {
        // make sure to check for errors
        return response.json();
      })
      .then(json => {
        this.setState(() => {
          return { data: json };
        });
      });
  }
  render() {
    return (
      <ul>
        {this.state.data.map(user => (
          <li key={user.name}>{user.name}</li>
        ))}
      </ul>
    );
  }
}
